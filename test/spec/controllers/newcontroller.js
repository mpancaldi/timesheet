'use strict';

describe('Controller: NewcontrollerCtrl', function () {

  // load the controller's module
  beforeEach(module('timesheetApp'));

  var NewcontrollerCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewcontrollerCtrl = $controller('NewcontrollerCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(NewcontrollerCtrl.awesomeThings.length).toBe(3);
  });
});
