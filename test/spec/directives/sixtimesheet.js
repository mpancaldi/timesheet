'use strict';

describe('Directive: sixTimesheet', function () {

  // load the directive's module
  beforeEach(module('timesheetApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<six-timesheet></six-timesheet>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the sixTimesheet directive');
  }));
});
