/**
 * Created by marta on 17/08/16.
 */


var app = angular.module("timesheetApp",['smart-table']);

  app.controller('smartCtrl', ['$scope', function ($scope) {
    $scope.rowCollection = [
      {firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'},
      {firstName: 'Blandine', lastName: 'Faivre', birthDate: new Date('1987-04-25'), balance: -2323.22, email: 'oufblandou@gmail.com'},
      {firstName: 'Francoise', lastName: 'Frere', birthDate: new Date('1955-08-27'), balance: 42343, email: 'raymondef@gmail.com'}
    ];

  }]);

  /*** Timesheet controller ***/

  app.controller('NumCtrl', ['$scope', function($scope) {

      var nums = [];
      var i = 1;

      $scope.d1 = Date.today().add(2).days();

      $scope.d = new Date(); // current date

      $scope.e1 = new Date(2016,7,20,0,0,0,0);


      var getDaysInMonth = function(month, year) {
        return new Date(year, month,0).getDate();
      };
      var daysInMonth = getDaysInMonth($scope.d.getMonth()+1, $scope.d.getYear()); // months: 1..12

      while(nums.push(i++) < daysInMonth); // Populate array up to the number of days in current month (e.g. 30)
      $scope.monthNums = nums;

      $scope.weekdays = ['Su','Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
      $scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];



      $scope.isNonWorkingDay = function(year, month, day) {
        var index = new Date(year, month-1, day).getDay();
        return (index == 6 || index == 0);

      };

      $scope.getWeekdayOf = function(year, month, day) {
        return $scope.weekdays[new Date(year, month-1, day).getDay()];
      };





      $scope.firstDayOfMonth = $scope.getWeekdayOf(2016, 8, 1); // Get which weekday is the 1st of current month (1..12)
    }]);


  app.controller('timeCtrl', ['$scope', function($scope) {
    $scope.a = new Date();
    $scope.b = new Date(2016,7,21);

    $scope.isNonWorkingDay = function(year, month, day) {
      var index = new Date(year, month-1, day).getDay();
      return (index == 6 || index == 0);

    }

  }]);

  app.controller('safeCtrl', ['$scope', function ($scope) {

    var firstnames = ['Laurent', 'Blandine', 'Olivier', 'Max'];
    var lastnames = ['Renard', 'Faivre', 'Frere', 'Eponge'];
    var dates = ['1987-05-21', '1987-04-25', '1955-08-27', '1966-06-06'];
    var id = 1;

    function generateRandomItem(id) {

      var firstname = firstnames[Math.floor(Math.random() * 3)];
      var lastname = lastnames[Math.floor(Math.random() * 3)];
      var birthdate = dates[Math.floor(Math.random() * 3)];
      var balance = Math.floor(Math.random() * 2000);

      return {
        id: id,
        firstName: firstname,
        lastName: lastname,
        birthDate: new Date(birthdate),
        balance: balance
      }
    }

    $scope.rowCollection = [];

    for (id; id < 5; id++) {
      $scope.rowCollection.push(generateRandomItem(id));
    }

    //add to the real data holder
    $scope.addRandomItem = function addRandomItem() {
      $scope.rowCollection.push(generateRandomItem(id));
      id++;
    };

    //remove to the real data holder
    $scope.removeItem = function removeItem(row) {
      var index = $scope.rowCollection.indexOf(row);
      if (index !== -1) {
        $scope.rowCollection.splice(index, 1);
      }
    }
  }]);

