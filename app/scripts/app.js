'use strict';

/**
 * @ngdoc overview
 * @name timesheetApp
 * @description
 * # timesheetApp
 *
 * Main module of the application.
 */
angular
  .module('timesheetApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'smart-table',
    'ui.bootstrap',
    'ui.bootstrap.tpls'
    /*,
    'mgcrea.ngStrap',
    'mgcrea.ngStrap.alert', 'mgcrea.ngStrap.modal', 'mgcrea.ngStrap.aside', 'mgcrea.ngStrap.tooltip'*/
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/timesheet', {
        templateUrl: 'test/timeSheetTest.html',
        //controller: 'NumCtrl',
        controllerAs: 'timesheet'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
