'use strict';

/**
 * @ngdoc function
 * @name timesheetApp.controller:NewcontrollerCtrl
 * @description
 * # NewcontrollerCtrl
 * Controller of the timesheetApp
 */
angular.module('timesheetApp')
  .controller('NewcontrollerCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  })

  .controller('newAppCtrl', function($scope) {

    $scope.first= "John";
    $scope.last= "Doe";

  })

  .controller('tabCtrl', ['$scope', function (scope) {
    scope.rowCollection = [
      {firstName: 'Laurent', lastName: 'Renard', birthDate: new Date('1987-05-21'), balance: 102, email: 'whatever@gmail.com'},
      {firstName: 'Blandine', lastName: 'Faivre', birthDate: new Date('1987-04-25'), balance: -2323.22, email: 'oufblandou@gmail.com'},
      {firstName: 'Francoise', lastName: 'Frere', birthDate: new Date('1955-08-27'), balance: 42343, email: 'raymondef@gmail.com'}
    ];
  }])


  .controller('NumCtrl', ['$scope', function($scope) {

    $scope.d = new Date(); // current date
    $scope.e = new Date(2016, 7, 20, 0, 0, 0);
    $scope.weekdays = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
    $scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    $scope.getDaysInMonth = function (month, year) {
      return new Date(year, month + 1, 0).getDate();
    };

    $scope.getWeekdayOf = function (year, month, day) {
      return $scope.weekdays[new Date(year, month, day).getDay()];
    };

    $scope.isToday = function (year, month, day) {
      var today = new Date();
      return (today.getFullYear() == year && today.getMonth() == month && today.getDate() == day)

    };

    var current = new Date();
    var maxDateVal = new Date(2026,0,1);
    var minDateVal = new Date(1999,11,31);
    $scope.current;
    var m = current.getMonth();
    var y = current.getFullYear();
    var daysInMonth2 = $scope.getDaysInMonth(m, y);
    var nums = [];
    var weekdayNames = [];
    var i = 1;
    while (nums.push(i++) < daysInMonth2); // Populate array up to the number of days in current month (e.g. 30)
    for (i = 1; i <= daysInMonth2; i++) weekdayNames.push($scope.getWeekdayOf(y, m, i));

    $scope.setMonthView = function (monthVal, yearVal) {
      current = new Date(yearVal, monthVal, 1);
      m = current.getMonth();
      y = current.getFullYear();
      daysInMonth2 = $scope.getDaysInMonth(m, y);
      nums = [];
      weekdayNames = [];
      i = 1;
      while (nums.push(i++) < daysInMonth2); // Populate array up to the number of days in current month (e.g. 30)
      for (i = 1; i <= daysInMonth2; i++) weekdayNames.push($scope.getWeekdayOf(y, m, i));
    };

    $scope.getMonthView = function () {
      return {
        month: m,
        year: y,
        monthNum: nums,
        date: current,
        weekdaysInMonth: weekdayNames
      }
    };

    $scope.firstDayOfMonth = $scope.getWeekdayOf(2016, 7, 1); // Get which weekday is the 1st of current month (0..11)

    $scope.selectView = function (date) {
      $scope.setMonthView(date.getMonth(), date.getFullYear());
    };

    $scope.content = "";
    $scope.test = function (msg) {
      $scope.content = msg;
    };

    /* 4-D array where
     * yi = year index (0.. starting from 2015)
     * mi = month index (0..11)
     * di = day index (0..30)
     * j = row index (0..n)
     */

    $scope.scheduleMatrix = new Array(5);
    for (var yi=0; yi<5; yi++) {
      $scope.scheduleMatrix[yi] = new Array(12);
      for (var mi = 0; mi < 12; mi++) {
        $scope.scheduleMatrix[yi][mi] = new Array(31);
        for (var di = 0; di < 31; di++) {
          $scope.scheduleMatrix[yi][mi][di] = new Array(3);
          for (var j = 0; j < 3; j++) {
            $scope.scheduleMatrix[yi][mi][di][j] = Math.floor(Math.random() * 3); //generate random integers 0..2 until method to insert/modify tasks is implemented
            if ($scope.scheduleMatrix[yi][mi][di][j] == 0) $scope.scheduleMatrix[yi][mi][di][j] = '&nbsp'; //Substitute value '0' with a space
            var day = new Date(yi+2015, mi, di);
            if (day.getDay() == 5 || day.getDay() == 6) $scope.scheduleMatrix[yi][mi][di][j] = '&nbsp'; //Non-working days do not include tasks
          }
        }
      }
    }

    $scope.hourVal = 0;

    $scope.saveToSchedule = function(date, di, row, hourVal) {
      $scope.scheduleMatrix [date.getFullYear()-2015] [date.getMonth()] [di-1] [row-1] = hourVal; //day count starts from 0 in html view
    };

    $scope.dynamicPopover = {
      content: 'Hello, World!',
      templateUrl: 'myPopoverTemplate.html',
      title: 'Title'
    };

    $scope.placement = {
      options: [
        'top',
        'top-left',
        'top-right',
        'bottom',
        'bottom-left',
        'bottom-right',
        'left',
        'left-top',
        'left-bottom',
        'right',
        'right-top',
        'right-bottom'
      ],
      selected: 'top'
    };

    /*
     * Datepicker Controller Methods
     */

    $scope.today = function() {
      $scope.current = new Date();
    };
    $scope.today();

    $scope.clear = function() {
      $scope.current = null;
    };

    $scope.inlineOptions = {
      customClass: getDayClass,
      minDate: new Date(),
      showWeeks: true
    };

    $scope.dateOptions = {
      dateDisabled: disabled,
      formatYear: 'yy',
      maxDate: new Date(2025, 11, 31),
      minDate: new Date(2000, 0, 1),
      startingDay: 1
    };

    // Disable weekend selection
    function disabled(data) {
      var date = data.date,
        mode = data.mode;
      return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function() {
      $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
      $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

    $scope.toggleMin();

    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
      $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
      $scope.current = new Date(year, month, day);
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
      opened: false
    };

    $scope.popup2 = {
      opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
      {
        date: tomorrow,
        status: 'full'
      },
      {
        date: afterTomorrow,
        status: 'partially'
      }
    ];

    function getDayClass(data) {
      var date = data.date,
        mode = data.mode;
      if (mode === 'day') {
        var dayToCheck = new Date(date).setHours(0,0,0,0);

        for (var i = 0; i < $scope.events.length; i++) {
          var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

          if (dayToCheck === currentDay) {
            return $scope.events[i].status;
          }
        }
      }
      return '';
    }

  }])

  .controller('safeCtrl', ['$scope', function ($scope) {

    var firstnames = ['Laurent', 'Blandine', 'Olivier', 'Max'];
    var lastnames = ['Renard', 'Faivre', 'Frere', 'Eponge'];
    var dates = ['1987-05-21', '1987-04-25', '1955-08-27', '1966-06-06'];
    var id = 1;

    function generateRandomItem(id) {

      var firstname = firstnames[Math.floor(Math.random() * 3)];
      var lastname = lastnames[Math.floor(Math.random() * 3)];
      var birthdate = dates[Math.floor(Math.random() * 3)];
      var balance = Math.floor(Math.random() * 2000);

      return {
        id: id,
        firstName: firstname,
        lastName: lastname,
        birthDate: new Date(birthdate),
        balance: balance
      }
    }

    $scope.rowCollection = [];

    for (id; id < 5; id++) {
      $scope.rowCollection.push(generateRandomItem(id));
    }

    //add to the real data holder
    $scope.addRandomItem = function() {
      $scope.rowCollection.push(generateRandomItem(id));
      id++;
    };

    //remove to the real data holder
    $scope.removeItem = function(row) {
      var index = $scope.rowCollection.indexOf(row);
      if (index !== -1) {
        $scope.rowCollection.splice(index, 1);
      }
    }
  }])

  .controller('PopoverDemoCtrl', function ($scope, $sce) {
    $scope.dynamicPopover = {
      content: 'Hello, World!',
      templateUrl: 'myPopoverTemplate.html',
      title: 'Title'
    };

    $scope.placement = {
      options: [
        'top',
        'top-left',
        'top-right',
        'bottom',
        'bottom-left',
        'bottom-right',
        'left',
        'left-top',
        'left-bottom',
        'right',
        'right-top',
        'right-bottom'
      ],
      selected: 'top'
    };

    //$scope.htmlPopover = $sce.trustAsHtml('<b style="color: red">I can</b> have <div class="label label-success">HTML</div> content');
  })

  .controller('Main', MainCtrl);

    MainCtrl.$inject = ['$uibModal'];
    function MainCtrl($uibModal) {
      var vm = this;

      vm.deleteModal = deleteModal;
      vm.people = [
        'Fred',
        'Jim',
        'Bob',
        'John'
      ];

      function deleteModal(person) {
        $uibModal.open({
          templateUrl: 'test/modal.html',
          controller: ['$uibModalInstance', 'people', 'person', DeleteModalCtrl],
          controllerAs: 'vm',
          resolve: {
            people: function () { return vm.people },
            person: function() { return person; }
          }
        });
      }
    }

    function DeleteModalCtrl($uibModalInstance, people, person) {
      var vm = this;

      vm.person = person;
      vm.deletePerson = deletePerson;

      function deletePerson() {
        people.splice(people.indexOf(person), 1);
        $uibModalInstance.close();
      }
}





