'use strict';

/**
 * @ngdoc function
 * @name timesheetApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the timesheetApp
 */
angular.module('timesheetApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  })

  .controller('NewAboutCtrl', function($scope)
    {
      $scope.a = new Date();

    }
  );
