'use strict';

/**
 * @ngdoc function
 * @name timesheetApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the timesheetApp
 */
angular.module('timesheetApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
