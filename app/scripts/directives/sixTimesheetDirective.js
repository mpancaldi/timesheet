'use strict';

/**
 * @ngdoc directive
 * @name timesheetApp.directive:newDirective
 * @description
 * # newDirective
 */
angular.module('timesheetApp')
  .directive('newDirective', function () {
    return {
      transclude: true,
      templateUrl: 'views/sixtimesheetTemplate.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        //element.text('this is the newDirective directive');
      }
    };
  });

