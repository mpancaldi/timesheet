'use strict';

/**
 * @ngdoc directive
 * @name timesheetApp.directive:sixTimesheet
 * @description
 * # sixTimesheet
 */
angular.module('timesheetApp')
  .directive('timesheetDir', function () {
    return {
      templateUrl: 'test/timeSheetTest.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
        element.text('this is the sixTimesheet directive');
      }
    };
  });
